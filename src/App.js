import './App.css';
import importedLogo from '@matt765/gitlab-package/build/logo512.png'

function App() {
  return (
    <div className="App">
      <img src={importedLogo} />
      This logo is imported from gitlab package
    </div>
  );
}

export default App;
